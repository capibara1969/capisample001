//
//  Util.swift
//  CapiSample001
//  共通処理クラス
//
//  Created by capibara1969 on 2021/02/06.
//  
//

import Foundation
import CoreData

class Util {
    
    /// Core Data コミット処理
    /// - Parameter context: NSManagedObjectContext
    class func commit(_ context: NSManagedObjectContext) {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
}
