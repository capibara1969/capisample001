//
//  sampleData.swift
//  CapiSample001
//
//  Created by capibara1969 on 2021/01/23.
//  
//

import Foundation
import CoreData

/// Personテーブル初期化処理
/// - Parameter context: NSManagedObjectContext
func initSamplePersons(context: NSManagedObjectContext ) {

    /// Personテーブル初期値
    let personList = [
        ["001", "北島康介", "1982/9/22", "man"],
        ["002", "羽生結弦", "1994/12/7", "man"],
        ["003", "荒川静香", "1981/12/29", "woman"],
        ["004", "高橋尚子", "1972/5/6", "woman"],
        ["005", "室伏広治", "1974/10/8", "man"]
    ]

    /// Date変換形式指定
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy/M/d"

    /// Personテーブル全消去
    deleteAllPersons(context: context)
    
    /// 初期データ登録
    for person in personList {
        let newPerson = Person(context: context)
        newPerson.personId = person[0]  // 人物ID
        newPerson.name = person[1]      // 名前
        newPerson.birthDate = dateFormatter.date(from: person[2])   // 生年月日
        newPerson.gender = person[3]    // 性別
    }

    Util.commit(context)
}


/// Parsonテーブルの全消去
/// - Parameter context: NSManagedObjectContext
func deleteAllPersons(context: NSManagedObjectContext) {
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Person")
    do {
        let persons = try context.fetch(fetchRequest) as! [Person]
        for person in persons {
            context.delete(person)
        }
    } catch {
        let nsError = error as NSError
        fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
    }
}
