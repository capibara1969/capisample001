//
//  ContentView.swift
//  CapiSample001
//
//  Created by capibara1969 on 2021/01/23.
//  
//

import SwiftUI
import CoreData

struct ContentView: View {
    @State private var showingAddPersonSheet = false
    
    /// NSManagedObjectContextの取得
    @Environment(\.managedObjectContext) private var context

    /// データの取得
    @FetchRequest(
        entity: Person.entity(),                    // ①
        sortDescriptors: [NSSortDescriptor(keyPath: \Person.personId, ascending: true)],    // ②
        animation: .default)                        // ③
    private var persons: FetchedResults<Person>     // ④
    
    var body: some View {
        NavigationView {
            List {
                ForEach(persons) { person in
                    HStack {
                        Text("\(person.personId!)")
                        Text("\(person.name!)")
                        Text("(\(dateToString(person.birthDate!)))")
                        Text("\(person.gender!)")
                    }
                }
                .onDelete(perform: deletePerson(offsets:))
            }
            .navigationTitle("Core Data サンプル")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    /// Personテーブル追加ボタン
                    Button(action: {showingAddPersonSheet = true}) {
                        Image(systemName: "plus")
                    }
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    /// Personテーブル初期化ボタン
                    Button(action: {initSamplePersons(context: context)}) {
                        Label("初期化", systemImage: "arrow.counterclockwise")
                    }
                }
            }
        }
        .sheet(isPresented: $showingAddPersonSheet) {
            AddPersonView()
        }
    }
    
    /// Personテーブルレコード削除処理
    /// - Parameter offsets: 削除対象の要素番号をIndexSet形式で渡す
    func deletePerson(offsets: IndexSet) {
        for index in offsets {
            context.delete(persons[index])
        }
        Util.commit(context)
    }
    
    /// 日付→文字列変換
    /// - Parameter date: Date
    /// - Returns: "yyyy/M/d" 形式の文字列
    func dateToString(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/M/d"

        return dateFormatter.string(from: date)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
