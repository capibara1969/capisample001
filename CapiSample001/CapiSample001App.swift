//
//  CapiSample001App.swift
//  CapiSample001
//
//  Created by capibara1969 on 2021/01/23.
//  
//

import SwiftUI

@main
struct CapiSample001App: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
