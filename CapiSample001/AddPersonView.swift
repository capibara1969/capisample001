//
//  AddPersonView.swift
//  CapiSample001
//
//  Created by capibara1969 on 2021/02/03.
//  
//

import SwiftUI

struct AddPersonView: View {
    @Environment(\.presentationMode) var presentationMode

    /// NSManagedObjectContextの取得
    @Environment(\.managedObjectContext) private var context
    
    @State private var personId = ""
    @State private var name = ""
    @State private var birthDate = Date()
    @State private var gender = "man"
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("入力フォーム")) {
                    TextField("ID", text: $personId)
                    TextField("名前", text: $name)
                    DatePicker("生年月日", selection: $birthDate, displayedComponents: .date)
                    Picker(selection: $gender, label: Text("性別")) {
                        Text("man").tag("man")
                        Text("woman").tag("woman")
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                
                Section(header: Text("操作")) {
                    Button("登録") {
                        /// 登録処理
                        addPerson()
                        presentationMode.wrappedValue.dismiss()     // sheetを閉じる
                    }
                    
                    Button("キャンセル") {
                        // キャンセル処理
                        presentationMode.wrappedValue.dismiss()     // sheetを閉じる
                    }
                }
            }
            .navigationTitle("新規登録")
        }
    }
    
    /// Personテーブル新規登録
    func addPerson() {
        let newPerson = Person(context: context)
        newPerson.personId = personId   // 人物ID
        newPerson.name = name           // 名前
        newPerson.birthDate = birthDate // 生年月日
        newPerson.gender = gender       // 性別

        Util.commit(context)
    }
}

struct AddPersonView_Previews: PreviewProvider {
    static var previews: some View {
        AddPersonView()
    }
}
